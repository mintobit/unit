<?php


class SelectRegExpTest extends PHPUnit_Framework_TestCase 
{
    protected function setUp()
    {
        $this->xml = new DomDocument;
        $this->xml->loadXML('<foo><bar>Baz</bar><bar>Baz</bar></foo>');
    }

    public function testAbsenceSuccess()
    {
        $this->assertSelectRegExp('foo bar', '/\d+/', FALSE, $this->xml);
    }

    public function testPresenceSuccess()
    {
        $this->assertSelectRegExp('foo bar', '/B[oea]z/', TRUE, $this->xml);
    }

    public function testExactCountSuccess()
    {
        $this->assertSelectRegExp('foo bar', '/Ba.*/', 2, $this->xml);
    }

    public function testRangeSuccess()
    {
        $this->assertSelectRegExp('foo bar', '/Ba.*/', array('>'=>0, '<'=>8), $this->xml);
    }
}
