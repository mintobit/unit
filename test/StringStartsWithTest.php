<?php


class StringStartsWithTest extends PHPUnit_Framework_TestCase 
{
    public function testSuccess()
    {
        $this->assertStringStartsWith('prefix', 'prefixfoo');
    }
}
