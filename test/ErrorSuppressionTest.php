<?php

class ErrorSuppressionTest extends PHPUnit_Framework_TestCase 
{
    public function testFileWriting()
    {
        $writer = new FileWriter();
        $this->assertFalse(@$writer->write('/non-writable/file', 'sample string'));
    }
}

class FileWriter
{
    public function write($file, $content)
    {
        $file = fopen($file, 'w');
        return (false === $file) ? $file : true;
    }
}
