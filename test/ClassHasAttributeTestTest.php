<?php

class ClassHasAttributeTestTest extends PHPUnit_Framework_TestCase
{
    public function testClassHasStaticAttribute()
    {
        $this->assertClassHasStaticAttribute('_foo', 'StaticClass');
    }
}

class StaticClass
{
    protected static $_foo;
}
