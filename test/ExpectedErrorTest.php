<?php

class ExpectedErrorTest extends PHPUnit_Framework_TestCase 
{
    public function testFailingInclude()
    {
        $this->setExpectedException('PHPUnit_Framework_Error');
        include 'nonexistent_file.php';
    }
}
