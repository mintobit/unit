<?php


class SelectCountTest extends PHPUnit_Framework_TestCase 
{
    protected function setUp()
    {
        $this->xml = new DomDocument;
        $this->xml->loadXML('<foo><bar/><bar/><bar/></foo>');
    }

    public function testAbsenceSuccess()
    {
        $this->assertSelectCount('bar foo', FALSE, $this->xml);
    }

    public function testPresenceSuccess()
    {
        $this->assertSelectCount('foo bar', TRUE, $this->xml);
    }

    public function testExactCountSuccess()
    {
        $this->assertSelectCount('foo bar', 3, $this->xml);
    }

    public function testRangeSuccess()
    {
        $this->assertSelectCount('foo bar', array('>'=>1, '<'=>8), $this->xml);
    }
}
