<?php


class StringEndsWithTest extends PHPUnit_Framework_TestCase 
{
    public function testSuccess()
    {
        $this->assertStringEndsWith('suffix', 'foosuffix');
    }
}
