<?php

class ExceptionTest extends PHPUnit_Framework_TestCase
{
    public function testException()
    {
        $this->setExpectedException('InvalidArgumentException');
        throw new InvalidArgumentException();
    }

    public function testExceptionHasRightMessage()
    {
        $this->setExpectedException('InvalidArgumentException', 'right message');
        throw new InvalidArgumentException('The very right message');
    }

    public function testExceptionHasRightCode()
    {
        $this->setExpectedException('InvalidArgumentException', 'right message', 20);
        throw new InvalidArgumentException('The very right message', 20);
    }
}
