<?php

class ContainsOnlyTest extends PHPUnit_Framework_TestCase
{
    public function testContainsOnly()
    {
        $this->assertContainsOnly('string', array('1', '3', '4'));
    }
}