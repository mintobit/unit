<?php


class JsonStringEqualsJsonStringTest extends PHPUnit_Framework_TestCase 
{
    public function testSuccess()
    {
        $this->assertJsonStringEqualsJsonString(json_encode(array('id' => 15)), json_encode(array('id' => 15)));
    }
}
