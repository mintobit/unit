<?php

class ClassHasAttributeTest extends PHPUnit_Framework_TestCase
{
    public function testClassHasAttribute()
    {
        $this->assertClassHasAttribute('code', 'Exception');
    }
}
