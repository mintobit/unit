<?php


class SelectEqualsTest extends PHPUnit_Framework_TestCase 
{
    protected function setUp()
    {
        $this->xml = new DomDocument;
        $this->xml->loadXML('<foo><bar>Baz</bar><bar>Baz</bar></foo>');
    }

    public function testAbsenceSuccess()
    {
        $this->assertSelectEquals('foo bar', 'Bar', FALSE, $this->xml);
    }

    public function testPresenceSuccess()
    {
        $this->assertSelectEquals('foo bar', 'Baz', TRUE, $this->xml);
    }

    public function testExactCountSuccess()
    {
        $this->assertSelectEquals('foo bar', 'Baz', 2, $this->xml);
    }

    public function testRangeSuccess()
    {
        $this->assertSelectEquals('foo bar', 'Baz', array('>'=>1, '<'=>3), $this->xml);
    }
}
