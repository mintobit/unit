<?php

class ContainsOnlyInstanceOfTest extends PHPUnit_Framework_TestCase
{
    public function testContainsOnlyInstanceOf()
    {
        $this->assertContainsOnlyInstancesOf('stdClass', array(new stdClass(), new stdClass()));
    }
}