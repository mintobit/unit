<?php

class ContainsTest extends PHPUnit_Framework_TestCase
{
    public function testArrContains()
    {
        $this->assertContains('needle', array('needle', 'foo'));
    }

    public function testStrContains()
    {
        $this->assertContains('needle', 'stringwithneedlestring');
    }
}