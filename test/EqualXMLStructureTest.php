<?php

class EqualXMLStructureTest extends PHPUnit_Framework_TestCase
{
    public function testEqualNodeNames()
    {
        $expected = new DOMElement('foo');
        $actual = new DOMElement('foo');

        $this->assertEqualXMLStructure($expected, $actual);
    }

    public function testEqualNodeAttributes()
    {
        $expected = new DOMDocument;
        $expected->loadXML('<foo bar="true" />');

        $actual = new DOMDocument;
        $actual->loadXML('<foo bar="true" />');

        $this->assertEqualXMLStructure(
            $expected->firstChild, $actual->firstChild, TRUE
        );
    }

    public function testEqualChildrenCount()
    {
        $expected = new DOMDocument;
        $expected->loadXML('<foo><bar/><bar/><bar/></foo>');

        $actual = new DOMDocument;
        $actual->loadXML('<foo><bar/><bar/><bar/></foo>');

        $this->assertEqualXMLStructure(
            $expected->firstChild, $actual->firstChild
        );
    }

    public function testEqualChildren()
    {
        $expected = new DOMDocument;
        $expected->loadXML('<foo><bar/><bar/><bar/></foo>');

        $actual = new DOMDocument;
        $actual->loadXML('<foo><bar/><bar/><bar/></foo>');

        $this->assertEqualXMLStructure(
            $expected->firstChild, $actual->firstChild
        );
    }
}
