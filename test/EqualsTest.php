<?php

class EqualsTest extends PHPUnit_Framework_TestCase
{
    public function testSuccess()
    {
        $this->assertEquals(1, 1);
    }

    public function testSuccess2()
    {
        $this->assertEquals('bar', 'bar');
    }

    public function testSuccess3()
    {
        $this->assertEquals("foo\nbar\nbaz\n", "foo\nbar\nbaz\n");
    }

    public function testSuccess4()
    {
        $this->assertEquals(1.0, 1.1, '', 0.2);
    }

    public function testFailure()
    {
        $this->assertNotEquals(1.0, 1.1);
    }

    public function testFailure2()
    {
        $expected = new DOMDocument;
        $expected->loadXML('<foo><bar/></foo>');

        $actual = new DOMDocument;
        $actual->loadXML('<bar><foo/></bar>');

        $this->assertNotEquals($expected, $actual);
    }

    public function testFailure3()
    {
        $expected = new stdClass;
        $expected->foo = 'foo';
        $expected->bar = 'bar';

        $actual = new stdClass;
        $actual->foo = 'bar';
        $actual->baz = 'bar';

        $this->assertNotEquals($expected, $actual);
    }

    public function testFailure4()
    {
        $this->assertNotEquals(array('a', 'b', 'c'), array('a', 'c', 'd'));
    }
}
