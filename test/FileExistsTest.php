<?php


class FileExistsTest extends PHPUnit_Framework_TestCase 
{
    public function testSuccess()
    {
        $this->assertFileNotExists('/path/to/file');
    }
}