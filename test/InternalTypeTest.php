<?php


class InternalTypeTest extends PHPUnit_Framework_TestCase 
{
    public function testSuccess()
    {
        $this->assertInternalType('int', 42);
    }
}